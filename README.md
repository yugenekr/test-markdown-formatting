# Test Lists with a Code Block

`Sample Line No List` 
```
Contents of the Code Block
Contents of the Code Block 2
```

* `Sample List Line, Code Block is Not Broken Anymore`
```
Contents of the Code Block
Contents of the Code Block 2
```
* `Sample List Line, Code Block with Indentation is Not Broken`
  ```
  Contents of the Code Block
  Contents of the Code Block 2
  ```
* `Sample List Line, Code Block with an Empty Line Before It That Used to be a fix`

```
Contents of the Code Block
Contents of the Code Block 2
```




